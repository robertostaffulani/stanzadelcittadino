<?php

namespace AppBundle\Protocollo;


class ProtocolloEvents
{
    const ON_PROTOCOLLA_PRATICA_SUCCESS = 'ocsdc.protocollo.on_protocolla_pratica_success';

    const ON_PROTOCOLLA_RICHIESTE_INTEGRAZIONE_SUCCESS = 'ocsdc.protocollo.on_protocolla_richieste_integrazione_success';

    const ON_PROTOCOLLA_ALLEGATI_INTEGRAZIONE_SUCCESS = 'ocsdc.protocollo.on_protocolla_allegati_success';

    const ON_PROTOCOLLA_ALLEGATI_OPERATORE_SUCCESS = 'ocsdc.protocollo.on_protocolla_allegati_operatore_success';
}