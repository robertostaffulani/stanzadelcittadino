# This repository adheres to the publiccode.yml standard by including this
# metadata file that makes public software easily discoverable.
# More info at https://github.com/italia/publiccode.yml

publiccodeYmlVersion: '0.2'
applicationSuite: Sito comunale
name: La Stanza del Cittadino
url: 'https://gitlab.com/opencontent/stanzadelcittadino'
landingURL: 'https://link.opencontent.it/sdc-home'
releaseDate: '2021-06-21'
softwareVersion: 1.7.2
developmentStatus: stable
softwareType: standalone/web
platforms:
  - web
usedBy:
  - Comune di Ala (TN)
  - Comune di Aldeno (TN)
  - Comune di Aamblar Don (TN)
  - Comune di Andalo (TN)
  - Comune di Arco (TN)
  - Comune di Avio (TN)
  - Comune di Bieno (TN)
  - Comune di Bleggio Superiore (TN)
  - Comune di Bocenago (TN)
  - Comune di Bondone (TN)
  - Comune di Borgo Lares (TN)
  - Comune di Caderzone (TN)
  - Comune di Campodenno (TN)
  - Comune di Canal San Bovo (TN)
  - Comune di Castel Condino (TN)
  - Comune di Cavedine (TN)
  - Comune di Cimone (TN)
  - Comune di Comano Terme (TN)
  - Comune di Fiavè (TN)
  - Comune di Folgaria (TN)
  - Comune di Garniga Terme (TN)
  - Comune di Imer (TN)
  - Comune di Lavarone (TN)
  - Comune di Ledro (TN)
  - Comune di Luserna (TN)
  - Comune di Maniago (PN)
  - Comune di Mazzin (TN)
  - Comune di Mezzano (TN)
  - Comune di Moena (TN)
  - Comune di Mori (TN)
  - Comune di Novella (TN)
  - Comune di Pelugo (TN)
  - Comune di Pergine (TN)
  - Comune di Pieve Tesino (TN)
  - Comune di Porte di Rendena (TN)
  - Comune di Predaia (TN)
  - Comune di Riva del Garda (TN)
  - Comune di Rovereto (TN)
  - Comune di Ruffrè Mendola (TN)
  - Comune di Sarnonico (TN)
  - Comune di Spiazzo (TN)
  - Comune di Spormione (TN)
  - Comune di Stenico (TN)
  - Comune di Storo (TN)
  - Comune di Strembo (TN)
  - Comune di Telve (TN)
  - Comune di Telve di Sopra (TN)
  - Comune di Terre D'Adige (TN)
  - Comune di Tre Ville (TN)
  - Comune di Trento (TN)
  - Comune di Vallelaghi (TN)
  - Comune di Villa Lagarina (TN)
  - Comune di Spormaggiore (TN)
  - Comune di Baselga di Pinè (TN)
  - Fondazione Mach (TN)
  - Provincia autonoma di Trento (TN)
  - Comune di Niscemi (CL)
  - Comune di Preganziol (TV)
  - Comune di Castelnuovo (TN)
  - Comune di Mezzolomabardo (TN)
  - Comune di Seriate (BG)
  - Comune di Monopoli (BA)
  - Comune di Verbania (VB)
roadmap: https://link.opencontent.it/sdc-roadmap
categories:
  - communications
  - crm
  - digital-citizenship
dependsOn:
  open:
    - name: PHP
      versionMin: '7.3'
    - name: PostgreSQL
      versionMin: '10'
    - name: Apache
      versionMin: '2.4'
    - name: Shibboleth
      versionMin: '3'
    - name: Docker
      versionMin: '18.03'
      optional: true
maintenance:
  type: community
  contacts:
    - name: Gabriele Francescotto
      phone: +39 0461 917437
      affiliation: Opencontent SCARL
      email: gabriele.francescotto@opencontent.it
legal:
  license: AGPL-3.0-or-later
  mainCopyrightOwner: Opencontent scarl
  repoOwner: Opencontent scarl
intendedAudience:
  scope:
    - local-authorities
  countries:
    - it
localisation:
  localisationReady: true
  availableLanguages:
    - it
it:
  piattaforme:
    spid: true
    pagopa: true
  conforme:
    lineeGuidaDesign: true
    gdpr: true
    misureMinimeSicurezza: true
    modelloInteroperabilita: true
description:
  it:
    genericName: ' L’area personale del cittadino'
    apiDocumentation: 'https://link.opencontent.it/sdc-apidoc'
    documentation: 'https://link.opencontent.it/sdc-manuale'
    shortDescription: >-
      Creazione moduli compilabili online e pratiche digitali con
      backoffice per gli operatori. Gestione Appuntamenti presso gli uffici o
      online.
    longDescription: >
      L’area personale con cui il cittadino può inviare istanze (es. iscrizioni
      asilo nido), verificare lo stato delle pratiche, ricevere comunicazioni da
      parte dell’ente (messaggi, scadenze, ...) ed effettuare pagamenti.
      Progettata con lo starter KIT per il sito di un comune insieme al Team per
      la Trasformazione Digitale, permette di creare nuovi servizi digitali
      secondo un flusso definito nelle Linee Guida di Design e integrato con le
      piattaforme abilitanti. Si integra facilmente con le applicazioni presenti
      presso l’ente (interoperabile, separando back end e front end); il profilo
      del cittadino si arricchisce ad ogni interazione e riduce la necessità di
      chiedere più volte le stesse informazioni (once only). Una soluzione
      pratica per gestire il processo di trasformazione digitale in linea col
      Piano Triennale. Nasce da un progetto condiviso con il Consorzio Comuni
      Trentini - ANCI Trentino, che accompagna i comuni nella revisione dei
      servizi al cittadino in un’ottica digitale (digital first)
    features:
      - Gestione Appuntamenti, per singole persone, per Uffici e per sportelli con più operatori.
      - Possibilità di approvare/spostare appuntamenti e gestire orari multipli di apertura.
      - Interfaccia responsive (Bootstrap Italia) conforme Linee Guida di design per i servizi web della PA
      - Autenticazione con SPID o Social e possibilità di aprire servizi anche senza autenticazione
      - Possibilità di scrivere al cittadino dal backoffice (con ricevuta di presa visione e PEC)
      - Integrazione con PagoPA attraverso MyPAY o altri gateway di pagamento regionali
      - Meccanismo di compilazione on-line dei moduli a step, con possibilità di salvare bozze
      - Gestione completa dell’iter. Invio, presa in carico, risposta al cittadino, richiesta integrazioni
      - Controllo della validità di firme digitali e formati dei file, per la conservazione sostitutiva
      - Generazione automatica del modulo compilato con ricevuta di invio dell’istanza, in formato PDF
      - Integrazione con il sito comunale Comunweb per prossime scadenze e descrizione dei servizi
      - Integrazione con i sistemi di protocollazione più diffusi (PiTre, Infor, Maggioli).
      - Possibilità di raccogliere servizi diversi in unico fascicolo
      - Pubblicazione documenti nell'area riservata del cittadino (cert. fiscali, tassa rifiuti, multe)
      - Creazione da parte di tecnici comunali di form complessi, con integrazione di API esterne
      - Integrazione completa con software open-source form.io per generazione dinamica dei moduli online
    screenshots:
      - images/sdc001.jpg
      - images/sdc002.jpg
      - images/sdc003.jpg
      - images/sdc004.jpg
      - images/sdc005.jpg
      - images/sdc006.jpg
      - images/sdc007.jpg
      - images/sdc007.jpg
      - images/sdc008.jpg
      - images/sdc009.jpg
      - images/sdc010.jpg
      - images/sdc011.jpg

