#!/usr/bin/env php
<?php

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Yaml\Yaml;

// if you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
//umask(0000);

set_time_limit(0);

/**
 * @var Composer\Autoload\ClassLoader $loader
 */
$loader = require __DIR__.'/../app/autoload.php';

$input = new ArgvInput();
$env = $input->getParameterOption(['--env', '-e'], getenv('SYMFONY_ENV') ?: 'dev');
$instance = $input->getParameterOption(['--instance', '-i'], '');

$debug = getenv('SYMFONY_DEBUG') !== '0' && !$input->hasParameterOption(['--no-debug', '']) && $env !== 'prod';

if ($debug) {
  Debug::enable();
}

if (!empty($instance)) {
  $instances = Yaml::parse(file_get_contents(__DIR__.'/../app/instances_'.$env.'.yml'));
  $instanceParams = $instances['instances'];

  $params = false;
  foreach ($instanceParams as $k => $v) {
    if ($v['identifier'] == $instance) {
      $params = $v;
    }
  }

  if (!$params) {
    throw new Exception("Instance $instance not found");
  }
  $kernel = new InstanceKernel($env, $debug);
  $kernel->setIdentifier($instance);
  $kernel->setInstanceParameters($params);
} else {
  $kernel = new AppKernel($env, $debug);
}


$application = new Application($kernel);

// Aggiungo l'opzione instance all'input definition altrimenti da errore
$inputDefinition = $application->getDefinition();
$inputDefinition->addOption(
  new InputOption('instance', 'i', InputOption::VALUE_OPTIONAL, 'Name of the Instance', null)
);

$application->run($input);
